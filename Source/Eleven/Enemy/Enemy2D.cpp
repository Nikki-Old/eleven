// Fill out your copyright notice in the Description page of Project Settings.

#include "Enemy/Enemy2D.h"
#include "PaperFlipbookComponent.h"
#include "PaperFlipbook.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"

#include "ActorComponent/CustomCharacterMovementComponent.h"
#include "ElevenGameModeBase.h"

DEFINE_LOG_CATEGORY_STATIC(LogCharacter_DooX, All, All);

AEnemy2D::AEnemy2D(const FObjectInitializer& ObjInit)
	: Super(ObjInit.SetDefaultSubobjectClass<UCustomCharacterMovementComponent>(AEnemy2D::CharacterMovementComponentName))
{
	// Create Flipbook component:
	FlipbookComponent = CreateDefaultSubobject<UPaperFlipbookComponent>(TEXT("FlipbookComponent"));
	FlipbookComponent->SetupAttachment(GetRootComponent());

	// Off collision on flipbook component:
	FlipbookComponent->SetCollisionProfileName("NoCollision");

	// Set Collision Profile:
	GetCapsuleComponent()->SetCollisionProfileName("AI_Pawn");

	// Mesh is not needed
	GetMesh()->DestroyComponent();

	CustomMovementComponent = Cast<UCustomCharacterMovementComponent>(GetMovementComponent());
	if (CustomMovementComponent)
	{
		CustomMovementComponent->OnWalking.AddDynamic(this, &AEnemy2D::Character2DOnWalking);
		CustomMovementComponent->OnFalling.AddDynamic(this, &AEnemy2D::Character2DOnFalling);
	}

	AttackDelay_Delegate.BindLambda([this]() { SetStateFlipbookComponent(EFlipbookState::Idle_State);
		SetCanAttack(true); });

	EndSpawn_Delegate.BindLambda([this]() { SetStateFlipbookComponent(EFlipbookState::Idle_State); });
	Death_Delegate.BindLambda([this]() { this->Destroy(); });
}

bool AEnemy2D::IsDead() const
{
	return bIsDead;
}

void AEnemy2D::BeginPlay()
{
	DefaultRotation = FlipbookComponent->GetRelativeRotation();
	InitEnemy();

	Super::BeginPlay();
}

void AEnemy2D::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// If is not Dead...
	if (!bIsDead)
	{
		MovementTick(DeltaTime);
	}
}

void AEnemy2D::InitEnemy()
{
	if (GetWorld())
	{
		UpdateEnemyParameters();
	}
}

void AEnemy2D::SetStateFlipbookComponent(EFlipbookState NewFlipbookState)
{
	float NewSpeed = 0.0f;

	switch (NewFlipbookState)
	{
		case EFlipbookState::Idle_State:
			if (IdleFlipbook)
			{
				FlipbookComponent->SetFlipbook(IdleFlipbook);
				NewSpeed = 0.0f;

				break;
			}

		case EFlipbookState::Attack_State:
			if (AttackFlipbook)
			{
				FlipbookComponent->SetFlipbook(AttackFlipbook);
				NewSpeed = 0.0f;
				break;
			}

		case EFlipbookState::Run_State:
			if (MovementFlipbook)
			{
				FlipbookComponent->SetFlipbook(MovementFlipbook);
				NewSpeed = MovementSpeed;
				break;
			}

		case EFlipbookState::Death_State:
			if (MovementFlipbook)
			{
				FlipbookComponent->SetFlipbook(DeathFlipbook);
				NewSpeed = 0.0f;
				break;
			}
		case EFlipbookState::Spawn_State:
			if (SpawnFlipbook)
			{
				FlipbookComponent->SetFlipbook(SpawnFlipbook);
				NewSpeed = 0.0f;
				break;
			}
	}

	CurrentFlipbookState = NewFlipbookState;

	if (CustomMovementComponent)
	{
		CustomMovementComponent->MaxWalkSpeed = NewSpeed;
	}
}

void AEnemy2D::SpawnFX_Implementation()
{
	if (!GetWorld())
	{
		return;
	}

	SetStateFlipbookComponent(EFlipbookState::Spawn_State);

	GetWorld()->GetTimerManager().SetTimer(
		EndSpawn_Timer,						  //
		EndSpawn_Delegate, SpawnTime, false); //
}

void AEnemy2D::MovementTick(float DeltaTime)
{
	if (CurrentTarget)
	{
		SetRotationToTarget(CurrentTarget);
	}
}

void AEnemy2D::SetRotationToTarget(const AActor* Target)
{
	if (Target)
	{
		// Find look at Rotation between THIS Actor and Target:
		auto LookAtRotation = UKismetMathLibrary::FindLookAtRotation(this->GetActorLocation(), Target->GetActorLocation());

		// Add offset:
		auto NewRotationYaw = (UKismetMathLibrary::ComposeRotators(LookAtRotation, RotationToTargetOffset)).Yaw;

		// Set Rotation:
		if (FlipbookComponent)
		{
			FlipbookComponent->SetWorldRotation(FRotator(DefaultRotation.Pitch, NewRotationYaw, DefaultRotation.Roll));
		}
	}
}

void AEnemy2D::SetTarget(AActor* NewTarget)
{
	CurrentTarget = NewTarget;
}

void AEnemy2D::SetCanAttack(bool Can)
{
	bIsCanAttack = Can;
}

AActor* AEnemy2D::GetTarget() const
{
	return CurrentTarget;
}

bool AEnemy2D::AttackTarget_Implementation(AActor* Target)
{
	if (bIsCanAttack)
	{
		if (Target && GetWorld())
		{
			// Current distance between this character and Target:
			const auto Distance = UKismetMathLibrary::Vector_Distance(this->GetActorLocation(), Target->GetActorLocation());

			if (Distance <= AttackDistance)
			{
				Target->TakeDamage(DamageAttack, FDamageEvent(), nullptr, this);

				bIsCanAttack = false;

				GetWorld()->GetTimerManager().SetTimer(AttackDelay_Timer, AttackDelay_Delegate, AttackFrequency, false);

				SetStateFlipbookComponent(EFlipbookState::Attack_State);

				return true;
			}
		}
	}

	return false;
}

void AEnemy2D::CharacterDeath_Implementation()
{
	if (!GetWorld())
	{
		return;
	}

	SetStateFlipbookComponent(EFlipbookState::Death_State);

	GetWorld()->GetTimerManager().SetTimer(Death_Timer, Death_Delegate, TimeAfterDeath, false);
}

void AEnemy2D::UpdateEnemyParameters_Implementation()
{
	// Set MovementSpeed:
	GetCharacterMovement()->MaxWalkSpeed = MovementSpeed;
}

void AEnemy2D::Character2DOnFalling()
{
	if (FlipbookComponent)
	{
		FlipbookComponent->Stop();
	}
}

void AEnemy2D::Character2DOnWalking()
{
	if (FlipbookComponent)
	{
		if (!FlipbookComponent->IsPlaying())
		{
			FlipbookComponent->Play();
		}
	}
}