// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "FuncLibrary/Types.h"
#include "Enemy2D.generated.h"

class UPaperFlipbookComponent;
class UPaperFlipbook;
class UCustomCharacterMovementComponent;
class AElevenGameModeBase;

UCLASS()
class ELEVEN_API AEnemy2D : public ACharacter
{
	GENERATED_BODY()

public:
	AEnemy2D(const FObjectInitializer& ObjInit);

	/** Flipbook Component */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Enemy2D")
	UPaperFlipbookComponent* FlipbookComponent = nullptr;

	/** Set Current Target: */
	UFUNCTION(BlueprintCallable, Category = "Enemy2D | Target")
	void SetTarget(AActor* NewTarget);

	UFUNCTION(BlueprintCallable, Category = "Enemy2D")
	void SetCanAttack(bool Can);

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Enemy2D | Parameters")
	float MovementSpeed = 600.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Enemy2D | Parameters")
	float AttackDistance = 200.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Enemy2D | Parameters")
	float DamageAttack = 10.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Enemy2D | Parameters")
	float AttackFrequency = 1.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Enemy2D | Parameters")
	float SpawnTime = 1.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Enemy2D | Parameters")
	float TimeAfterDeath = 1.0f;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Enemy2D")
	bool IsDead() const;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void InitEnemy();

	// Flipbook:

	/** Idle Paper Flipbook */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Enemy2D | State")
	UPaperFlipbook* IdleFlipbook = nullptr;

	/** Attack Paper Flipbook */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Enemy2D | State")
	UPaperFlipbook* AttackFlipbook = nullptr;

	/** Movement Paper Flipbook */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Enemy2D | State")
	UPaperFlipbook* MovementFlipbook = nullptr;

	/** Death Paper Flipbook */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Enemy2D | State")
	UPaperFlipbook* DeathFlipbook = nullptr;

	/** Spawn Paper Flipbook */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Enemy2D | State")
	UPaperFlipbook* SpawnFlipbook = nullptr;

	/** Each state correspons a specific flipbook. */
	UFUNCTION(BlueprintCallable, Category = "Enemy2D | State")
	void SetStateFlipbookComponent(EFlipbookState NewFlipbookState);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Enemy2D")
	void SpawnFX();
	virtual void SpawnFX_Implementation();

	// Movement:
	/** Movement tick */
	void MovementTick(float DeltaTime);

	/***/
	UFUNCTION(BlueprintCallable, Category = "Enemy2D | Movement")
	void SetRotationToTarget(const AActor* Target);

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Enemy2D | Movement")
	FRotator RotationToTargetOffset = FRotator(0);

	/** Get Current Target */
	UFUNCTION(BlueprintCallable, Category = "Enemy2D | Target")
	AActor* GetTarget() const;

	// Attack Target:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Enemy2D")
	bool AttackTarget(AActor* Target);
	virtual bool AttackTarget_Implementation(AActor* Target);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Enemy2D")
	void CharacterDeath();
	virtual void CharacterDeath_Implementation();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Enemy2D | Target")
	void UpdateEnemyParameters();
	void UpdateEnemyParameters_Implementation();

protected:

private:
	bool bIsDead = false;
	bool bIsCanAttack = true;

	AActor* CurrentTarget = nullptr;

	FRotator DefaultRotation = FRotator(0);

	EFlipbookState CurrentFlipbookState = EFlipbookState::NONE_State;

	UCustomCharacterMovementComponent* CustomMovementComponent = nullptr;

	UFUNCTION()
	void Character2DOnFalling();
	UFUNCTION()
	void Character2DOnWalking();

	AElevenGameModeBase* GameMode = nullptr;

	FTimerHandle AttackDelay_Timer;
	FTimerDelegate AttackDelay_Delegate;

	FTimerHandle EndSpawn_Timer;
	FTimerDelegate EndSpawn_Delegate;

	FTimerHandle Death_Timer;
	FTimerDelegate Death_Delegate;
};
