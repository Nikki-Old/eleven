// Fill out your copyright notice in the Description page of Project Settings.

#include "Character/Character2D.h"
#include "PaperFlipbookComponent.h"
#include "Components/WidgetComponent.h"
#include "PaperFlipbook.h"

#include "PlayerController/PCBase.h"
#include "UI/DescriptionWidget.h"

ACharacter2D::ACharacter2D()
{
	// Create inventory slots:
	InventorySlotOne = CreateDefaultSubobject<USceneComponent>(TEXT("SlotOne"));
	InventorySlotOne->SetupAttachment(GetMesh());

	InventorySlotTwo = CreateDefaultSubobject<USceneComponent>(TEXT("SlotTwo"));
	InventorySlotTwo->SetupAttachment(GetMesh());

	// Create flipbook component:
	FlipbookComponent = CreateDefaultSubobject<UPaperFlipbookComponent>(TEXT("FlipbookComp"));
	FlipbookComponent->SetupAttachment(GetRootComponent());

	// Create widget component:
	WidgetComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("WidgetComponent"));
	WidgetComponent->SetupAttachment(GetRootComponent());
	WidgetComponent->SetWidgetSpace(EWidgetSpace::Screen);

	if (IsValid(DescriptionWidgetClass))
	{
		WidgetComponent->SetWidgetClass(DescriptionWidgetClass);
	}
}

void ACharacter2D::NotifyCanInteractWithActor(AActor * InteractActor, FActorInfo ActorInfo)
{
	if (InteractActor)
	{
		if (WidgetComponent)
		{
			auto Widget = Cast<UDescriptionWidget>(WidgetComponent->GetUserWidgetObject());
			if (Widget)
			{
				Widget->SetVisibility(ESlateVisibility::Visible);
				Widget->UpdateDescriptin(ActorInfo);
			}
		}

		if (PlayerController)
		{
			PlayerController->NotifyCanInteractWithActor(InteractActor);
		}
	}
}

void ACharacter2D::ClearIteractActor()
{
	if (WidgetComponent)
	{
		auto Widget = Cast<UDescriptionWidget>(WidgetComponent->GetUserWidgetObject());
		if (Widget)
		{
			Widget->SetVisibility(ESlateVisibility::Collapsed);
		}

		if (PlayerController)
		{
			PlayerController->ClearInteractActor();
		}
	}
}

void ACharacter2D::SetMovementState(EMovementState2D NewMovementState)
{
	if (NewMovementState == MovementState)
	{
		// Do nothing
		return;
	}

	switch (NewMovementState)
	{
		case EMovementState2D::Idle_State:
			MovementState = EMovementState2D::Idle_State;
			FlipbookComponent->SetFlipbook(IdleFlipbook);

			break;

		case EMovementState2D::Back_State:
			MovementState = EMovementState2D::Back_State;
			FlipbookComponent->SetFlipbook(BackFlipbook);

			break;

		case EMovementState2D::Forward_State:
			MovementState = EMovementState2D::Forward_State;
			FlipbookComponent->SetFlipbook(ForwardFlipbook);

			break;

		case EMovementState2D::Left_State:
			MovementState = EMovementState2D::Left_State;
			FlipbookComponent->SetFlipbook(LeftFlipbook);

			break;

		case EMovementState2D::Right_State:
			MovementState = EMovementState2D::Right_State;
			FlipbookComponent->SetFlipbook(RightFlipbook);

			break;
	}
}

void ACharacter2D::MoveForward(float Value)
{
	if (Value > 0)
	{
		SetMovementState(EMovementState2D::Back_State);
	}
	else if (Value < 0)
	{
		SetMovementState(EMovementState2D::Forward_State);
	}

	Super::MoveForward(Value);
}

void ACharacter2D::MoveRight(float Value)
{
	if (Value > 0)
	{
		SetMovementState(EMovementState2D::Right_State);
	}
	else if (Value < 0)
	{
		SetMovementState(EMovementState2D::Left_State);
	}

	Super::MoveRight(Value);
}

void ACharacter2D::PossessedBy(AController* NewController)
{
	if (NewController)
	{
		PlayerController = Cast<APCBase>(NewController);
	}

	Super::PossessedBy(NewController);
}

void ACharacter2D::BeginPlay()
{
	Super::BeginPlay();

	// Set Idle State:
	SetMovementState(EMovementState2D::Idle_State);

	// Hide Widget:
	auto Widget = Cast<UDescriptionWidget>(WidgetComponent->GetUserWidgetObject());
	if (Widget)
	{
		Widget->SetVisibility(ESlateVisibility::Collapsed);
	}
}

void ACharacter2D::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (FMath::IsNearlyZero(GetVelocity().Size()))
	{
		SetMovementState(EMovementState2D::Idle_State);
	}
}
