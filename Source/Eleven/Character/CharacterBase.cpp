// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterBase.h"

// Sets default values
ACharacterBase::ACharacterBase()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACharacterBase::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ACharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ACharacterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Movement:
	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &ACharacterBase::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &ACharacterBase::MoveRight);
	PlayerInputComponent->BindAction(TEXT("Jump"), IE_Pressed, this, &ACharacterBase::StartJump);

}

void ACharacterBase::MoveForward(float Value)
{
	if (FMath::IsNearlyZero(Value))
		return;

	this->AddMovementInput(FVector(1.0f, 0.0f, 0.0f), Value);
}

void ACharacterBase::MoveRight(float Value)
{
	if (FMath::IsNearlyZero(Value))
		return;

	this->AddMovementInput(FVector(0.0f, 1.0f, 0.0f), Value);
}

//bool ACharacterBase::IsFalling() const
//{
//	return false;
//}

void ACharacterBase::StartJump()
{
	Jump();
}

