// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Character/CharacterBase.h"
#include "FuncLibrary/Types.h"
#include "Character2D.generated.h"

/**
 *
 */

class APCBase;
class UPaperFlipbookComponent;
class UPaperFlipbook;
class UWidgetComponent;
class UUserWidget;
class UDescriptionWidget;

UCLASS()
class ELEVEN_API ACharacter2D : public ACharacterBase
{
	GENERATED_BODY()

public:
	ACharacter2D();

	// Inventory:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Character2D")
	USceneComponent* InventorySlotOne = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Character2D")
	USceneComponent* InventorySlotTwo = nullptr;

	/** Widget comonent for display discription what to do */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Character2D")
	UWidgetComponent* WidgetComponent = nullptr;

	/** Flipbook Component */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Character2D")
	UPaperFlipbookComponent* FlipbookComponent = nullptr;

	/** Idle Paper Flipbook */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Character2D")
	UPaperFlipbook* IdleFlipbook = nullptr;

	/** Back Move Paper Flipbook */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Character2D")
	UPaperFlipbook* BackFlipbook = nullptr;

	/** Forward Move Paper Flipbook */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Character2D")
	UPaperFlipbook* ForwardFlipbook = nullptr;

	/** Left Move Paper Flipbook */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Character2D")
	UPaperFlipbook* LeftFlipbook = nullptr;

	/** Right Move Paper Flipbook */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Character2D")
	UPaperFlipbook* RightFlipbook = nullptr;

	// Interact:
	UFUNCTION(BlueprintCallable, Category = "Character2D | Interact")
	void NotifyCanInteractWithActor(AActor * InteractActor, FActorInfo ActorInfo);

	UFUNCTION(BlueprintCallable, Category = "Character2D | Interact")
	void ClearIteractActor();

	// Movement:

	UFUNCTION(BlueprintCallable, Category = "Character2D")
	void SetMovementState(EMovementState2D NewMovementState);

	/***/
	virtual void MoveForward(float Value) override;
	virtual void MoveRight(float Value) override;

	virtual void PossessedBy(AController* NewController) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Character2D | DescriptionWidget")
	TSubclassOf<UDescriptionWidget> DescriptionWidgetClass;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	/** Movement State : */
	EMovementState2D MovementState = EMovementState2D::NONE_State;

	APCBase* PlayerController = nullptr;
};
