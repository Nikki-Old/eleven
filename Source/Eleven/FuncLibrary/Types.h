// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"
#include "Types.generated.h"

/**
 * 
 */
class AItemActor;

UENUM(BlueprintType)
enum class EItemType : uint8
{
	NONE_Type UMETA(DisplayName = "NONE Type"),
	Big_Type UMETA(DisplayName = "Big Type"),
	Normal_Type UMETA(DisplayName = "Normal Type")
};

USTRUCT(BlueprintType)
struct FItemActorInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ItemActorInfo")
	FName ItemName = "";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ItemActorInfo")
	FName DevItemName = "";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ItemActorInfo")
	UStaticMesh* Mesh = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ItemActorInfo")
	TSubclassOf<AItemActor> ItemClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ItemActorInfo")
	EItemType ItemType = EItemType::NONE_Type;

	FItemActorInfo()
	{
		ItemName = "";
		Mesh = nullptr;
		ItemType = EItemType::NONE_Type;
	}

	FItemActorInfo(FName ItemName, UStaticMesh* Mesh, TSubclassOf<AItemActor> ItemClass, EItemType ItemType)
	{
		this->ItemName = ItemName;
		this->Mesh = Mesh;
		this->ItemClass = ItemClass;
		this->ItemType = ItemType;
	}

	bool IsEmpty() const
	{
		if (ItemName.IsNone() && !Mesh && ItemType == EItemType::NONE_Type)
		{
			return true;
		}

		return false;
	}
};

UENUM(BlueprintType)
enum class EMouseState : uint8
{
	NONE_State UMETA(DisplayName = "NONE State"),
	Default_State UMETA(DisplayName = "Default State"),
	Interact_State UMETA(DisplayName = "Interact State")
};

/** Movement state.*/
UENUM(BlueprintType)
enum class EMovementState2D : uint8
{

	NONE_State UMETA(DisplayName = "NONE State"),
	Idle_State UMETA(DisplayName = "Idle State"),
	Back_State UMETA(DisplayName = "Back State"),
	Forward_State UMETA(DisplayName = "Forward State"),
	Left_State UMETA(DisplayName = "Left State"),
	Right_State UMETA(DisplayName = "Right State")
};

UENUM(BlueprintType)
enum class EFlipbookState : uint8
{
	NONE_State UMETA(DisplayName = "None"),
	Idle_State UMETA(DisplayName = "Idle"),
	Run_State UMETA(DisplayName = "Run"),
	Death_State UMETA(DisplayName = "Death"),
	Spawn_State UMETA(DisplayName = "Spawn"),
	Attack_State UMETA(DisplayName = "Attack")
};

USTRUCT(BlueprintType)
struct FActorInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ItemActorInfo")
	FName Name = "";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ItemActorInfo")
	FText Description = FText();
};

USTRUCT(BlueprintType)
struct FSpawnItemsInformation : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Cargo", meta = (AllowPrivateAccess = "true"))
	FName ItemName;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Cargo", meta = (AllowPrivateAccess = "true"))
	int32 CountItem;

	FSpawnItemsInformation()
	{
		ItemName = "";
		CountItem = 0;
	}

	FSpawnItemsInformation(FName ItemName, int32 CountItem)
	{
		this->ItemName = ItemName;
		this->CountItem = CountItem;
	}

	bool IsEmpty()
	{
		if (ItemName.IsNone() && CountItem == 0)
		{
			return true;
		}

		return false;
	}
};

UCLASS()
class ELEVEN_API UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
};
