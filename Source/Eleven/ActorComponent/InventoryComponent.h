// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "FuncLibrary/Types.h"
#include "InventoryComponent.generated.h"

class ACharacter2D;
class AItemActor;

UCLASS(Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class ELEVEN_API UInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UInventoryComponent();

	//UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "Inventory")
	//USceneComponent* InventorySlotOne = nullptr;

	//UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "Inventory")
	//USceneComponent* InventorySlotSecond = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Inventory | Take/Drop")
	float DistanceToStopFlyItem = 100.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Inventory | Take/Drop")
	float CoefPowerImpulse = 1.0f;

	UFUNCTION(BlueprintCallable, Category = "Inventory | Take/Drop")
	bool DropLastItem(FVector DropLocation);

	UFUNCTION(BlueprintCallable, Category = "Inventory | Take/Drop")
	bool DropItemFromMainSlot(FVector DropLocation);

	UFUNCTION(BlueprintCallable, Category = "Inventory | Take/Drop")
	void SwitchItemBetweenSlots();

	UFUNCTION(BlueprintCallable, Category = "Inventory")
	void AddItemInfo(FItemActorInfo NewItemInfo);

	UFUNCTION(BlueprintCallable, Category = "Inventory | Take/Drop")
	void TakeItemToInventory(AItemActor* NewItem);

	UFUNCTION(BlueprintPure, Category = "Inventory")
	TArray<FItemActorInfo> GetItemsInfo() const;

	UFUNCTION(BlueprintCallable, Category = "Inventory")
	bool PopFirstItemInfo(FItemActorInfo& ItemInfo);


	UFUNCTION(BlueprintCallable, Category = "Inventory")
	USceneComponent* GetFreeSlotForItem(AItemActor*& FreeSlot, bool bIsDropOldItem = false);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Inventory")
	AItemActor* GetItemFromMainSlot() const;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "Inventory")
	TArray<FItemActorInfo> ItemsInfo;

	UFUNCTION()
	void GetInventorySlotsByOwner();

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	UPROPERTY()
	ACharacter2D* OwnerCharacter = nullptr;


	TPair<USceneComponent*, AItemActor*> InventorySlotMain;

	TPair<USceneComponent*, AItemActor*> InventorySlotSupp;
};
