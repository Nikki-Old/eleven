// Fill out your copyright notice in the Description page of Project Settings.

#include "ActorComponent/InventoryComponent.h"
#include "Containers/Map.h"

#include "Item/ItemActor.h"
#include "Character2D.h"

// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

bool UInventoryComponent::DropLastItem(FVector DropLocation)
{
	if (GetWorld())
	{
		if (ItemsInfo.Num() > 0)
		{
			const int32 LastIndex = ItemsInfo.Num() - 1;

			const auto LastItemInfo = ItemsInfo[LastIndex];

			if (IsValid(LastItemInfo.ItemClass))
			{
				// TO DO: Add Sound...

				// Spawn Parameters:
				FVector SpawnLocation = InventorySlotMain.Key->GetComponentLocation();
				FRotator SpawnRotation = InventorySlotMain.Key->GetComponentRotation();
				//FActorSpawnParameters DropItemSpawnParameters;
				//DropItemSpawnParameters.Owner = nullptr;
				//DropItemSpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

				auto NewItem = GetWorld()->SpawnActorDeferred<AItemActor>(LastItemInfo.ItemClass, FTransform(SpawnRotation, SpawnLocation));
				if (NewItem)
				{
					NewItem->SetItemInfo(LastItemInfo);
					NewItem->FinishSpawning(FTransform(SpawnRotation, SpawnLocation));
					NewItem->DropItem(DropLocation, CoefPowerImpulse);
				}
			}
		}
	}

	return false;
}

bool UInventoryComponent::DropItemFromMainSlot(FVector DropLocation)
{
	if (InventorySlotMain.Key && InventorySlotMain.Value)
	{
		InventorySlotMain.Value->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		InventorySlotMain.Value->DropItem(DropLocation, CoefPowerImpulse);
		InventorySlotMain.Value = nullptr;

		return true;
	}

	if (InventorySlotSupp.Key && InventorySlotSupp.Value)
	{
		InventorySlotSupp.Value->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		InventorySlotSupp.Value->DropItem(DropLocation, CoefPowerImpulse);
		InventorySlotSupp.Value = nullptr;

		return true;
	}

	return false;
}

void UInventoryComponent::SwitchItemBetweenSlots()
{
	if (!InventorySlotMain.Value && !InventorySlotSupp.Value)
	{
		return;
	}

	const auto Buffer = InventorySlotMain.Value;
	InventorySlotMain.Value = InventorySlotSupp.Value;
	if (InventorySlotMain.Value)
	{
		InventorySlotMain.Value->StartTakeItem(InventorySlotMain.Key, DistanceToStopFlyItem, false);
	}

	InventorySlotSupp.Value = Buffer;
	if (InventorySlotSupp.Value)
	{
		InventorySlotSupp.Value->StartTakeItem(InventorySlotSupp.Key, DistanceToStopFlyItem, false);
	}
}

void UInventoryComponent::AddItemInfo(FItemActorInfo NewItemInfo)
{
	ItemsInfo.Add(NewItemInfo);
}

void UInventoryComponent::TakeItemToInventory(AItemActor* NewItem)
{
	if (NewItem)
	{
		bool bIsDestroyAfterTake = false;
		bool bIsNeedFindInventorySlot = false;

		const auto ItemInfo = NewItem->GetItemActorInfo();
		switch (ItemInfo.ItemType)
		{
			case EItemType::Big_Type:
				bIsDestroyAfterTake = false;
				bIsNeedFindInventorySlot = true;
				break;

			case EItemType::Normal_Type:
				bIsDestroyAfterTake = true;
				bIsNeedFindInventorySlot = false;
				break;
		}

		if (bIsNeedFindInventorySlot)
		{
			USceneComponent* FreeSlot = GetFreeSlotForItem(NewItem, true);
			if (FreeSlot)
			{
				NewItem->StartTakeItem(FreeSlot, DistanceToStopFlyItem, bIsDestroyAfterTake);
			}
		}
		else
		{
			NewItem->StartTakeItem(InventorySlotMain.Key, DistanceToStopFlyItem, bIsDestroyAfterTake);
			ItemsInfo.Add(NewItem->GetItemActorInfo());
		}
	}
}

TArray<FItemActorInfo> UInventoryComponent::GetItemsInfo() const
{
	return ItemsInfo;
}

bool UInventoryComponent::PopFirstItemInfo(FItemActorInfo& ItemInfo)
{
	if (ItemsInfo.Num() > 0)
	{
		const auto FirstItemInfo = ItemsInfo[0];
		ItemsInfo.RemoveAt(0);
		ItemInfo = FirstItemInfo;
		return true;
	}
	else
	{
		return false;
	}
}

USceneComponent* UInventoryComponent::GetFreeSlotForItem(AItemActor*& FreeSlot, bool bIsDropOldItem)
{

	if (!InventorySlotMain.Value)
	{
		InventorySlotMain.Value = FreeSlot;

		return InventorySlotMain.Key;
	}

	if (!InventorySlotSupp.Value)
	{
		InventorySlotSupp.Value = FreeSlot;

		return InventorySlotSupp.Key;
	}

	if (bIsDropOldItem)
	{
		if (InventorySlotMain.Value)
		{
			// TO DO: added logic for where drop item?
			InventorySlotMain.Value->DropItem(FVector(0), 0.0f);
			InventorySlotMain.Value = nullptr;
			InventorySlotMain.Value = FreeSlot;
			return InventorySlotMain.Key;
		}
	}

	return nullptr;
}

AItemActor* UInventoryComponent::GetItemFromMainSlot() const
{
	return InventorySlotMain.Value;
}

// Called when the game starts
void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	GetInventorySlotsByOwner();
}

void UInventoryComponent::GetInventorySlotsByOwner()
{
	if (GetOwner())
	{
		OwnerCharacter = Cast<ACharacter2D>(GetOwner());

		InventorySlotMain.Key = OwnerCharacter->InventorySlotOne;
		InventorySlotSupp.Key = OwnerCharacter->InventorySlotTwo;
	}
}

// Called every frame
void UInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}
