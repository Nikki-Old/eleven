// Fill out your copyright notice in the Description page of Project Settings.


#include "ActorComponent/CustomCharacterMovementComponent.h"

void UCustomCharacterMovementComponent::SetMovementMode(EMovementMode NewMovementMode, uint8 NewCustomMode)
{
	Super::SetMovementMode(NewMovementMode, NewCustomMode);

	switch (NewMovementMode)
	{
		case EMovementMode::MOVE_Falling:
			OnFalling.Broadcast();
			break;
		case EMovementMode::MOVE_Walking:
			OnWalking.Broadcast();
			break;
	}
}