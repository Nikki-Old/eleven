// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "CustomCharacterMovementComponent.generated.h"

/**
 * 
 */

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWalking);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnFallingOwner);

UCLASS()
class ELEVEN_API UCustomCharacterMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()

public:
	UPROPERTY()
	FOnWalking OnWalking;

	UPROPERTY()
	FOnFallingOwner OnFalling;

private:
	virtual void SetMovementMode(EMovementMode NewMovementMode, uint8 NewCustomMode = 0) override;
	
};
