// Fill out your copyright notice in the Description page of Project Settings.

#include "Item/ItemActor.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"

#include "Actor/Other/AssemblyTable.h"
#include "ElevenGameModeBase.h"
#include "GameInstance/ElevenGameInstance.h"
#include "ActorComponent/InventoryComponent.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
AItemActor::AItemActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	this->SetRootComponent(MeshComponent);
	MeshComponent->SetSimulatePhysics(true);
	MeshComponent->OnComponentHit.AddDynamic(this, &AItemActor::MeshOnHit);

	InteractComponent = CreateDefaultSubobject<USphereComponent>(TEXT("InteractComponent"));
	InteractComponent->SetupAttachment(GetRootComponent());
	InteractComponent->SetCollisionProfileName("Iteract");
}

bool AItemActor::StartInteract_Implementation(AActor* Caller)
{
	if (Caller)
	{
		auto InventoryComponent = Cast<UInventoryComponent>(Caller->GetComponentByClass(UInventoryComponent::StaticClass()));

		if (InventoryComponent)
		{
			InventoryComponent->TakeItemToInventory(this);
			return true;
		}
	}

	return false;
}

void AItemActor::EndInteract_Implementation()
{
}

bool AItemActor::IsCanInteract_Implementation() const
{
	return bIsCanInteract;
}

void AItemActor::DropItem_Implementation(FVector Location, float CoefPowerImpulse)
{

	if (!GetWorld())
	{
		return;
	}

	GetWorld()->GetTimerManager().ClearTimer(TakeItem_Timer);
	//auto ProjectileMovementComp = Cast<UProjectileMovementComponent>(this->AddComponentByClass(UProjectileMovementComponent::StaticClass(), true, FTransform(), true));

	const auto Direction = UKismetMathLibrary::GetDirectionUnitVector(this->GetActorLocation(), Location);
	const auto PowerImpulse = UKismetMathLibrary::Vector_Distance(this->GetActorLocation(), Location) * CoefPowerImpulse;
	//ProjectileMovementComp->InitialSpeed = 3000.0f;
	//ProjectileMovementComp->Velocity = Direction * ProjectileMovementComp->InitialSpeed;

	this->SetActorEnableCollision(true);
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	MeshComponent->SetSimulatePhysics(true);

	MeshComponent->AddImpulse(Direction * PowerImpulse, FName(), true);

	bIsCanInteract = true;
}

void AItemActor::StartTakeItem_Implementation(USceneComponent* InventorySlot, float DistanceToStop, bool bIsAfterTakeDestroy)
{
	DistanceToAttach = DistanceToStop;
	this->bDestroyAfterTake = bIsAfterTakeDestroy;

	if (GetWorld())
	{
		if (InventorySlot)
		{
			TargetInventorySlot = InventorySlot;
			MeshComponent->SetSimulatePhysics(false);
			bIsCanInteract = false;
			this->SetActorEnableCollision(false);
			this->SetActorRotation(FRotator(0));

			GetWorld()->GetTimerManager().SetTimer(TakeItem_Timer, this, &AItemActor::FlyToInventorySlot, HowOftenUpdateTake, true);
		}
	}

	if (bOnTable)
	{
		TakeFromTable();
	}
}

FActorInfo AItemActor::GetActorInfo_Implementation() const
{
	FActorInfo ActorInfo;
	ActorInfo.Name = ItemActorInfo.ItemName;

	return ActorInfo;
}

void AItemActor::SetItemInfo(FItemActorInfo NewItemInfo)
{
	ItemActorInfo = NewItemInfo;
}

FItemActorInfo AItemActor::GetItemActorInfo() const
{
	return ItemActorInfo.IsEmpty() ? FItemActorInfo() : ItemActorInfo;
}

bool AItemActor::PutOnTable(UStaticMeshComponent* Mesh, FName FreeSocket)
{
	if (!Mesh || FreeSocket.IsNone() || bOnTable)
	{
		return false;
	}
	bOnTable = true;

	MeshComponent->SetSimulatePhysics(false);
	this->AttachToComponent(Mesh, FAttachmentTransformRules::KeepWorldTransform, FreeSocket);
	this->SetActorLocation(Mesh->GetSocketLocation(FreeSocket));

	if (bDoOnceCallDelegate_OnPutItem)
	{
		OnPutItem.Broadcast();
		bDoOnceCallDelegate_OnPutItem = false;
	}
	return true;
}

// Called when the game starts or when spawned
void AItemActor::BeginPlay()
{
	Super::BeginPlay();

	IntialItem();
}

void AItemActor::IntialItem()
{
	if (!GetWorld())
	{
		return;
	}

	if (ItemActorInfo.IsEmpty())
	{
		const auto GameInstance = Cast<UElevenGameInstance>(GetGameInstance());

		if (GameInstance)
		{
			ItemActorInfo = GameInstance->GetRandomItemInfo();
		}
	}

	if (bIsBroadcastGameMode)
	{
		auto GameMode = Cast<AElevenGameModeBase>(GetWorld()->GetAuthGameMode());
		if (GameMode)
		{
			GameMode->AddNewItem(this);
		}
	}

	if (ItemActorInfo.Mesh)
	{
		MeshComponent->SetStaticMesh(ItemActorInfo.Mesh);
	}
}

void AItemActor::PickUpSuccess_Implementation()
{
}

void AItemActor::MeshOnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor)
	{
		OtherActor->TakeDamage(100.0f, FDamageEvent(), nullptr, this);
		auto AssemblyTable = Cast<AAssemblyTable>(OtherActor);
		if (AssemblyTable)
		{
			AssemblyTable->PutItemOnTable(this);
		}
	}
}

// Called every frame
void AItemActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AItemActor::BeginDestroy()
{
	OnDestroyedItem.Broadcast(this);
	Super::BeginDestroy();
}

// TO DO: Take location
void AItemActor::FlyToInventorySlot()
{
	const FVector TargetLocation = TargetInventorySlot->GetComponentLocation();
	const FVector CurrentLocation = this->GetActorLocation();

	const FVector Direction = UKismetMathLibrary::GetDirectionUnitVector(CurrentLocation, TargetLocation);

	const float Distance = UKismetMathLibrary::Vector_Distance(CurrentLocation, TargetLocation);

	if (Distance <= DistanceToAttach)
	{
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TakeItem_Timer);
		}

		if (bDestroyAfterTake)
		{
			this->Destroy();
		}
		else
		{
			this->AttachToComponent(TargetInventorySlot, FAttachmentTransformRules::KeepRelativeTransform);
			this->SetActorRelativeLocation(FVector(0));
		}

		return;
	}

	this->AddActorWorldOffset(Direction * DistanceMovePerUpdated, true);
}

void AItemActor::FlyToLocation(FVector Location)
{
	const FVector TargetLocation = TargetInventorySlot->GetComponentLocation();
	const FVector CurrentLocation = this->GetActorLocation();

	const FVector Direction = UKismetMathLibrary::GetDirectionUnitVector(CurrentLocation, TargetLocation);

	const float Distance = UKismetMathLibrary::Vector_Distance(CurrentLocation, TargetLocation);

	if (Distance <= DistanceToAttach)
	{

		this->AttachToComponent(TargetInventorySlot, FAttachmentTransformRules::KeepRelativeTransform);
		this->SetActorRelativeLocation(FVector(0));

		this->SetActorRotation(FRotator(0));

		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TakeItem_Timer);
		}

		return;
	}

	this->AddActorWorldOffset(Direction * DistanceMovePerUpdated, true);
}

void AItemActor::TakeFromTable()
{
	this->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
	OnTakeFromTable.Broadcast(this);
	bOnTable = false;
}
