// Fill out your copyright notice in the Description page of Project Settings.

#include "Actor/Item/Weapon/Projectile/ProjectileBase.h"
#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AProjectileBase::AProjectileBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Sphere collision:
	SphereCollisionProjectile = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere")); // what touches with other actor's

	SphereCollisionProjectile->SetSphereRadius(15.0f);
	SphereCollisionProjectile->bReturnMaterialOnMove = true;	  //hit event return physMaterial
	SphereCollisionProjectile->SetCanEverAffectNavigation(false); //collision not affect navigation (P keybord on editor)
	// Set the sphere's collision profile name to "Projectile"
	SphereCollisionProjectile->BodyInstance.SetCollisionProfileName(TEXT("Projectile"));
	RootComponent = SphereCollisionProjectile;

	// Event's:
	SphereCollisionProjectile->OnComponentHit.AddDynamic(this, &AProjectileBase::ProjectileCollisionSphereHit);					  // Hit
	SphereCollisionProjectile->OnComponentBeginOverlap.AddDynamic(this, &AProjectileBase::ProjectileCollisionSphereBeginOverlap); // Begin Overlap
	SphereCollisionProjectile->OnComponentEndOverlap.AddDynamic(this, &AProjectileBase::ProjectileCollisionSphereEndOverlap);	  // EndOverlap

	// Static Mesh:
	StaticMeshProjectile = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static mesh component"));
	StaticMeshProjectile->SetupAttachment(RootComponent);
	StaticMeshProjectile->SetCanEverAffectNavigation(false);
	StaticMeshProjectile->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	// Projectile Movement:
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Projectile movement component"));
	// What object we update?
	ProjectileMovement->UpdatedComponent = RootComponent; //SetUpdatedComponent(RootComponent);

	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = true;

	ProjectileMovement->ProjectileGravityScale = 0.0f;
}

void AProjectileBase::InitialProjectile(float InitialSpeed)
{
	ProjectileMovement->InitialSpeed = InitialSpeed;
	//ProjectileMovement->Velocity = Velocity;
}

// Called when the game starts or when spawned
void AProjectileBase::BeginPlay()
{
	Super::BeginPlay();
}

void AProjectileBase::ProjectileCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor)
	{
		// Apply Damage:
		UGameplayStatics::ApplyPointDamage(OtherActor, ProjectileDamage, Hit.Location, Hit, GetInstigatorController(), GetOwner()->GetOwner(), NULL);
	}
}

void AProjectileBase::ProjectileCollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
}

void AProjectileBase::ProjectileCollisionSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
}

// Called every frame
void AProjectileBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AProjectileBase::ProjectileExplose()
{
	this->Destroy();
}
