// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Actor/Item/ItemActor.h"
#include "WeaponBase.generated.h"

/**
 * 
 */
class APCBase;
class UArrowComponent;
class AProjectileBase;

UCLASS()
class ELEVEN_API AWeaponBase : public AItemActor
{
	GENERATED_BODY()

public:
	AWeaponBase();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	UArrowComponent* ShootLocation = nullptr;

	/** Usage Interface */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Usage")
	bool StartUsage(AActor* Caller);
	bool StartUsage_Implementation(AActor* Caller);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Usage")
	void Usage();
	void Usage_Implementation();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Usage")
	void EndUsage();
	void EndUsage_Implementation();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Usage")
	bool IsUse();
	bool IsUse_Implementation();
	/** Usage Interface */

	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	void Fire();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	int32 MaxAmmo = 10;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon")
	TSubclassOf<AProjectileBase> ProjectileClass;

private:
	int32 CurrentAmmo = 0;

	APCBase* PlayerController = nullptr;
};
