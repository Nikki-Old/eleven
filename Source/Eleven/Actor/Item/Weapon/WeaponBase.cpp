// Fill out your copyright notice in the Description page of Project Settings.

#include "Actor/Item/Weapon/WeaponBase.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/ArrowComponent.h"

#include "Actor/Item/Weapon/Projectile/ProjectileBase.h"
#include "PlayerController/PCBase.h"

AWeaponBase::AWeaponBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(GetRootComponent());
}

bool AWeaponBase::StartUsage_Implementation(AActor* Caller)
{
	if (!Caller || !GetWorld())
	{
		return false;
	}

	if (GetWorld()->GetFirstPlayerController())
	{
		PlayerController = Cast<APCBase>(GetWorld()->GetFirstPlayerController());

		if (PlayerController)
		{
		}
	}

	bIsStartUsage = true;

	return true;
}

void AWeaponBase::Usage_Implementation()
{
	if (bIsStartUsage)
	{
		Fire();
	}
}

void AWeaponBase::EndUsage_Implementation()
{
	bIsStartUsage = false;

	this->SetActorRotation(FRotator(0));

	OnEndUsageItem.Broadcast(this);
}

bool AWeaponBase::IsUse_Implementation()
{
	return bIsStartUsage;
}

void AWeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (PlayerController)
	{
		if (bIsStartUsage)
		{
			const auto HitUnderCursor = PlayerController->GetHitUnderCursor();

			const auto LookAtRotation = UKismetMathLibrary::FindLookAtRotation(this->GetActorLocation(), HitUnderCursor.Location);

			this->SetActorRotation(LookAtRotation);
		}
	}
}

void AWeaponBase::BeginPlay()
{
	Super::BeginPlay();
}

void AWeaponBase::Fire()
{
	if (!GetWorld() || !IsValid(ProjectileClass))
	{
		return;
	}

	if (ShootLocation)
	{
		FTransform SpawnTransform;
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FVector Direction = ShootLocation->GetForwardVector();
		

		FMatrix myMatrix(Direction, FVector(0, 1, 0), FVector(0, 0, 1), FVector::ZeroVector);
		FRotator SpawnRotation = myMatrix.Rotator();

		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		SpawnParams.Owner = this;
		SpawnParams.Instigator = GetInstigator();

		SpawnTransform.SetLocation(SpawnLocation);
		SpawnTransform.SetRotation(SpawnRotation.Quaternion());
		SpawnTransform.SetScale3D(FVector(1.0f));

		auto NewProjectile = GetWorld()->SpawnActorDeferred<AProjectileBase>(ProjectileClass, SpawnTransform, this);
		if (NewProjectile)
		{
			NewProjectile->InitialProjectile(1000.0f);
			NewProjectile->SetLifeSpan(10.0f);
			NewProjectile->FinishSpawning(SpawnTransform);
		}
	}
}
