// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Interface/InteractInterface.h"
#include "Interface/UsageInterface.h"
#include "Interface/InformationInterface.h"
#include "FuncLibrary/Types.h"

#include "ItemActor.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnTakeFromTable, AItemActor*, Caller);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnDestroyedItem, AItemActor*, Caller);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnPutItem);

class USphereComponent;

UCLASS()
class ELEVEN_API AItemActor : public AActor, public IInteractInterface, public IUsageInterface, public IInformationInterface
{
	GENERATED_BODY()

public:
	// Delegates:
	UPROPERTY(BlueprintAssignable, EditAnyWhere, BlueprintReadWrite, Category = "ItemActor")
	FOnTakeFromTable OnTakeFromTable;

	UPROPERTY(BlueprintAssignable, EditAnyWhere, BlueprintReadWrite, Category = "ItemActor")
	FOnDestroyedItem OnDestroyedItem;

	UPROPERTY(BlueprintAssignable, EditAnyWhere, BlueprintReadWrite, Category = "ItemActor")
	FOnPutItem OnPutItem;

public:
	// Sets default values for this actor's properties
	AItemActor();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Mesh")
	UStaticMeshComponent* MeshComponent = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Mesh")
	USphereComponent* InteractComponent = nullptr;

	/** Interact Inteface: */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Item")
	bool StartInteract(AActor* Caller);
	bool StartInteract_Implementation(AActor* Caller);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interact")
	void EndInteract();
	void EndInteract_Implementation();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interact")
	bool IsCanInteract() const;
	bool IsCanInteract_Implementation() const;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "ItemActor")
	void StartTakeItem(USceneComponent* InventorySlot, float DistanceToStop, bool bIsAfterDestroy);
	void StartTakeItem_Implementation(USceneComponent* InventorySlot, float DistanceToStop, bool bIsAfterDestroy);
	/** Interact Inteface: */

	/** Information Interface */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interact")
	FActorInfo GetActorInfo() const;
	FActorInfo GetActor_Implementation() const;
	/** Information Interface */

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "ItemActor")
	void DropItem(FVector Location, float CoefPowerImpulse);
	void DropItem_Implementation(FVector Location, float CoefPowerImpulse);

	UFUNCTION(BlueprintCallable, Category = "ItemActor")
	void SetItemInfo(FItemActorInfo NewItemInfo);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "ItemActor")
	FItemActorInfo GetItemActorInfo() const;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ItemActor | FlyToInventory")
	float HowOftenUpdateTake = 0.01f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ItemActor | FlyToInventory")
	float DistanceMovePerUpdated = 1.0f;

	UFUNCTION(BlueprintCallable, Category = "ItemActor")
	bool PutOnTable(UStaticMeshComponent* Mesh, FName FreeSocket);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void IntialItem();

	UFUNCTION(BlueprintCallable, Category = "ItemActor")
	void TakeFromTable();

	/** This function need override: */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "ItemActor")
	void PickUpSuccess();
	virtual void PickUpSuccess_Implementation();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ItemActor")
	FItemActorInfo ItemActorInfo;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ItemActor")
	bool bIsBroadcastGameMode = true;

	UFUNCTION()
	void MeshOnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void BeginDestroy() override;

private:
	bool bIsCanInteract = true;
	bool bOnTable = false;
	bool bDestroyAfterTake = false;
	bool bDoOnceCallDelegate_OnPutItem = true;

	FTimerHandle TakeItem_Timer;

	USceneComponent* TargetInventorySlot = nullptr;

	UFUNCTION()
	void FlyToInventorySlot();

	float DistanceToAttach = 0.0f;

	UFUNCTION()
	void FlyToLocation(FVector Location);
};
