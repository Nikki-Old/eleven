// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FuncLibrary/Types.h"
#include "CargoActor.generated.h"

class UDestructibleComponent;
class UElevenGameInstance;
class AItemActor;

UCLASS()
class ELEVEN_API ACargoActor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ACargoActor();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Cargo")
	USceneComponent* SceneComponent = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Cargo", meta = (AllowPrivateAccess = "true"))
	TArray<FSpawnItemsInformation> SpawnItemsInformation;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Cargo", meta = (AllowPrivateAccess = "true"))
	int32 MaxCountItem = 5;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	bool GetItemInfo(FName ItemName, FItemActorInfo& ItemInfo);

	UFUNCTION(BlueprintCallable, Category = "Cargo")
	void SpawnItems();

private:
	UElevenGameInstance* GameInstance = nullptr;
};
