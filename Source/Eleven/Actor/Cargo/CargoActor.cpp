// Fill out your copyright notice in the Description page of Project Settings.

#include "Actor/Cargo/CargoActor.h"

#include "Actor/Item/ItemActor.h"
#include "GameInstance/ElevenGameInstance.h"

// Sets default values
ACargoActor::ACargoActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	// Create Scene Component:
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	this->SetRootComponent(SceneComponent);
}

// Called when the game starts or when spawned
void ACargoActor::BeginPlay()
{
	Super::BeginPlay();

	if (SpawnItemsInformation.Num() == 0)
	{
		if (!GameInstance)
		{
			GameInstance = Cast<UElevenGameInstance>(GetGameInstance());
		}

		if(GameInstance)
		{
			const auto NeedNames = FMath::RandRange(1, MaxCountItem);
			TArray<FName> Names;

			GameInstance->GetItemNames(NeedNames, Names);

			for (const auto Name : Names)
			{
				FSpawnItemsInformation SpawnItemInfo;
				SpawnItemInfo.ItemName = Name;
				SpawnItemInfo.CountItem = FMath::RandRange(1, (MaxCountItem / 2) - 1);
				
				SpawnItemsInformation.Add(SpawnItemInfo);
			}
		}
	}
}

bool ACargoActor::GetItemInfo(FName ItemName, FItemActorInfo& ItemInfo)
{
	if (!GetWorld() || ItemName.IsNone())
	{
		return false;
	}

	if (!GameInstance)
	{
		GameInstance = Cast<UElevenGameInstance>(GetGameInstance());
	}

	if (GameInstance)
	{
		GameInstance->GetItemInfoByName(ItemName, ItemInfo);
		return true;
	}

	return false;
}

// Called every frame
void ACargoActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ACargoActor::SpawnItems()
{
	if (!GetWorld())
	{
		return;
	}

	for (const auto SpawnItemInfo : SpawnItemsInformation)
	{
		FItemActorInfo NewItemInfo;

		if (!GetItemInfo(SpawnItemInfo.ItemName, NewItemInfo))
		{
			continue;
		}

		FTransform SpawnTransform;
		SpawnTransform.SetLocation(this->GetActorLocation());
		SpawnTransform.SetRotation(this->GetActorRotation().Quaternion());

		for (int32 i = 0; i < SpawnItemInfo.CountItem; i++)
		{
			auto NewItem = GetWorld()->SpawnActorDeferred<AItemActor>(NewItemInfo.ItemClass, //
				SpawnTransform,																   //
				this, nullptr,																   //
				ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn);		   //

			if (NewItem)
			{
				NewItem->SetItemInfo(NewItemInfo);
				NewItem->FinishSpawning(SpawnTransform, true);
			}
		}
	}
}

