// Fill out your copyright notice in the Description page of Project Settings.

#include "Actor/Other/AssemblyTable.h"
#include "Components/BoxComponent.h"
#include "ActorComponent/InventoryComponent.h"

#include "Interface/UsageInterface.h"
#include "Character/Character2D.h"
#include "Actor/Item/ItemActor.h"

// Sets default values
AAssemblyTable::AAssemblyTable()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	this->SetRootComponent(MeshComponent);

	//MeshComponent->GetAllSocketNames();

	CollisionComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("CollisionBox"));
	CollisionComponent->SetupAttachment(MeshComponent);
	CollisionComponent->OnComponentBeginOverlap.AddDynamic(this, &AAssemblyTable::CollisionComponentOnOverlapped);
	CollisionComponent->OnComponentEndOverlap.AddDynamic(this, &AAssemblyTable::CollisionComponentOnEndOverlapped);
	CollisionComponent->SetCollisionProfileName("AssemblyTable");
}

bool AAssemblyTable::PutItemOnTable(AItemActor* Item)
{
	if (!Item)
	{
		return false;
	}

	const auto FreeSocket = GetAvailableSoket();
	if (!FreeSocket.IsNone())
	{
		if (Item->PutOnTable(MeshComponent, FreeSocket))
		{
			MeshSockets[FreeSocket] = Item;
			Item->OnTakeFromTable.AddDynamic(this, &AAssemblyTable::ItemIsTakedFromTable);
		}
	}

	return false;
}

bool AAssemblyTable::StartInteract_Implementation(AActor* Caller)
{
	if (!Caller)
	{
		return false;
	}

	Inventory = Cast<UInventoryComponent>(Caller->GetComponentByClass(UInventoryComponent::StaticClass()));

	if (Inventory)
	{
		return LayingOutItems();
	}

	return false;
}

void AAssemblyTable::EndInteract_Implementation()
{
}

bool AAssemblyTable::IsCanInteract_Implementation() const
{
	return false;
}

// Called when the game starts or when spawned
void AAssemblyTable::BeginPlay()
{
	Super::BeginPlay();

	GetAllMeshSockets();
}

bool AAssemblyTable::LayingOutItems()
{
	if (!Inventory || !GetWorld())
	{
		return false;
	}

	FItemActorInfo ItemInfo;
	if (Inventory->PopFirstItemInfo(ItemInfo))
	{
		const auto FreeSlot = GetAvailableSoket();
		if (!FreeSlot.IsNone())
		{
			FTransform SpawnTransform;
			SpawnTransform.SetLocation(this->GetActorLocation());

			auto Item = GetWorld()->SpawnActorDeferred<AItemActor>(ItemInfo.ItemClass, //
				SpawnTransform, nullptr, nullptr,									   //
				ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn);   //

			if (Item)
			{
				Item->SetItemInfo(ItemInfo);
				Item->FinishSpawning(SpawnTransform);
				PutItemOnTable(Item);
				return true;
			}
		}
	}

	return false;
}

bool AAssemblyTable::CompareItemsWithOreder(TArray<FName> Order)
{
	int32 NumberNeedItems = Order.Num();

	for (const auto Socket : MeshSockets)
	{
		for (int32 i = 0; i < Order.Num(); i++)
		{
			if (Socket.Value)
			{
				if (Socket.Value->GetItemActorInfo().ItemName == Order[i])
				{
					NumberNeedItems--;
					Order.RemoveAt(i);
				}
			}
		}
	}

	return NumberNeedItems <= 0 ? true : false;
}

void AAssemblyTable::ClearAllMeshSockets()
{
	for (auto Socket : MeshSockets)
	{
		Socket.Value->DetachFromActor(FDetachmentTransformRules::KeepRelativeTransform);
		Socket.Value->Destroy();
	}
}

bool AAssemblyTable::MeshSocketAreBusy() const
{
	int32 NumSocketAreBusy = 0;
	for (const auto Socket : MeshSockets)
	{
		if (Socket.Value)
		{
			NumSocketAreBusy++;
		}
	}

	return NumSocketAreBusy == MeshSockets.Num() ? true : false;
}

// Called every frame
void AAssemblyTable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AAssemblyTable::GetAllMeshSockets()
{
	const TArray<FName> SoketNames = MeshComponent->GetAllSocketNames();

	for (const auto SoketName : SoketNames)
	{
		MeshSockets.Add(SoketName);
		MeshSockets[SoketName] = nullptr;
	}
}

FName AAssemblyTable::GetAvailableSoket() const
{
	for (const auto Socket : MeshSockets)
	{
		if (!Socket.Value)
		{
			return Socket.Key;
		}
	}

	return FName();
}

void AAssemblyTable::CollisionComponentOnOverlapped(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor)
	{
		const auto OtherItem = Cast<AItemActor>(OtherActor);
		if (OtherItem)
		{
			PutItemOnTable(OtherItem);
		}

		auto Player = Cast<ACharacter2D>(OtherActor);
		if (Player)
		{
			Player->NotifyCanInteractWithActor(this, ActorInfo);
		}
	}
}

void AAssemblyTable::CollisionComponentOnEndOverlapped(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor)
	{
		auto Player = Cast<ACharacter2D>(OtherActor);
		if (Player)
		{
			Player->ClearIteractActor();
		}
	}
}

void AAssemblyTable::ItemIsTakedFromTable(AItemActor* Item)
{
	if (Item)
	{
		for (auto& SoketInfo : MeshSockets)
		{
			if (SoketInfo.Value == Item)
			{
				SoketInfo.Value = nullptr;
				Item->OnTakeFromTable.RemoveDynamic(this, &AAssemblyTable::ItemIsTakedFromTable);
			}
		}
	}
}
