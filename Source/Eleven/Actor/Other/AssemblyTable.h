// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FuncLibrary/Types.h"
#include "Interface/InteractInterface.h"

#include "AssemblyTable.generated.h"

class UBoxComponent;
class AItemActor;
class UInventoryComponent;

UCLASS()
class ELEVEN_API AAssemblyTable : public AActor, public IInteractInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AAssemblyTable();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "AssemblyTable")
	UStaticMeshComponent* MeshComponent = nullptr;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "AssemblyTable")
	UBoxComponent* CollisionComponent = nullptr;

	UFUNCTION(BlueprintCallable, Category = "AssemblyTable")
	bool PutItemOnTable(AItemActor* Item);

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "AssemblyTable")
	FActorInfo ActorInfo;

	/** Usage Interface */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interact")
	bool StartInteract(AActor* Caller);
	bool StartInteract_Implementation(AActor* Caller);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interact")
	void EndInteract();
	void EndInteract_Implementation();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interact")
	bool IsCanInteract() const;
	bool IsCanInteract_Implementation() const;
	/** Usage Interface */

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "AssemblyTable")
	TMap<FName, AItemActor*> MeshSockets;

	UFUNCTION(BlueprintCallable, Category = "AssemblyTable")
	bool LayingOutItems();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "AssemblyTable")
	bool CompareItemsWithOreder(TArray<FName> Order);

	UFUNCTION(BlueprintCallable, Category = "AssemblyTable")
	void ClearAllMeshSockets();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "AssemblyTable")
	bool MeshSocketAreBusy() const;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	void GetAllMeshSockets();
	FName GetAvailableSoket() const;

	UFUNCTION()
	void CollisionComponentOnOverlapped(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void CollisionComponentOnEndOverlapped(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
	void ItemIsTakedFromTable(AItemActor* Item);

	UInventoryComponent* Inventory = nullptr;
};
