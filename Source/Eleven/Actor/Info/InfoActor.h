// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FuncLibrary/Types.h"

#include "InfoActor.generated.h"

class UUserWidget;
class UActorInfoWidget;
class UWidgetComponent;

UCLASS()
class ELEVEN_API AInfoActor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AInfoActor();

	/** Scene component: */
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "InfoActor")
	USceneComponent* SceneComponent = nullptr;

	/** Widget component: */
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "InfoActor")
	UWidgetComponent* InfoWidgetComponent = nullptr;

	UFUNCTION(BlueprintCallable, Category = "InfoActor")
	void UpdateDamageWidget(FItemActorInfo ItemInfo);

	UFUNCTION(BlueprintCallable, Category = "InfoActor")
	void Show(FActorInfo ActorInfomation);

	UFUNCTION(BlueprintCallable, Category = "InfoActor")
	void Hide();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/** Widget class: */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "InfoActor")
	TSubclassOf<UActorInfoWidget> InfoWidgetClass;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	UActorInfoWidget* InfoWidget = nullptr;
};
