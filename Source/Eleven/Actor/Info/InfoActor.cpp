// Fill out your copyright notice in the Description page of Project Settings.

#include "Actor/Info/InfoActor.h"
#include "Blueprint/UserWidget.h"
#include "Components/WidgetComponent.h"
#include "UI/ActorInfoWidget.h"

// Sets default values
AInfoActor::AInfoActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	this->SetRootComponent(SceneComponent);

	// Create WidgetComponent:
	InfoWidgetComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("InfoWidgetComponent"));
	InfoWidgetComponent->SetupAttachment(GetRootComponent());

	// Set Widget Class:
	if (IsValid(InfoWidgetClass))
	{
		InfoWidgetComponent->SetWidgetClass(InfoWidgetClass);
	}

	InfoWidgetComponent->SetWidgetSpace(EWidgetSpace::Screen);
}

void AInfoActor::UpdateDamageWidget(FItemActorInfo ItemInfo)
{

}

void AInfoActor::Show(FActorInfo ActorInfomation)
{
	if (InfoWidgetComponent)
	{
		if (!InfoWidgetComponent->IsVisible())
		{
			InfoWidgetComponent->SetHiddenInGame(false, true);
		}

		if (InfoWidget)
		{
			InfoWidget->UpdateInfo(ActorInfomation);
		}
	}
}

void AInfoActor::Hide()
{
	if (InfoWidgetComponent && InfoWidgetComponent->IsVisible())
	{
		InfoWidgetComponent->SetHiddenInGame(true, true);
	}
}

// Called when the game starts or when spawned
void AInfoActor::BeginPlay()
{
	Super::BeginPlay();

	InfoWidget = Cast<UActorInfoWidget>(InfoWidgetComponent->GetUserWidgetObject());
}

// Called every frame
void AInfoActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
