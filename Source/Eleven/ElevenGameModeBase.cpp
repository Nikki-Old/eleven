// Copyright Epic Games, Inc. All Rights Reserved.

#include "ElevenGameModeBase.h"

#include "Actor/Item/ItemActor.h"

void AElevenGameModeBase::RemoveItem(AItemActor* Item)
{
	if (Item)
	{
		const int32 IndexItem = ItemActors.Find(Item);
		if (IndexItem >= 0)
		{
			ItemActors.RemoveAt(IndexItem);

			OnItemDisappeared.Broadcast();
		}
	}
}

void AElevenGameModeBase::ReactionForItemOnPut_Implementation()
{
}

void AElevenGameModeBase::ReactionForItemOnAdded_Implementation()
{
}

void AElevenGameModeBase::BeginPlay()
{
	Super::BeginPlay();
}

TArray<FName> AElevenGameModeBase::GetItemNames(int32 Count) const
{
	TArray<FName> ItemNames;
	for (int32 i = 0; i < Count; i++)
	{
		const int32 RandomIndex = FMath::RandRange(0, ItemActors.Num() - 1);

		if (ItemActors.IsValidIndex(RandomIndex))
		{
			if (ItemActors[RandomIndex])
			{
				ItemNames.Add(ItemActors[RandomIndex]->GetItemActorInfo().ItemName);
			}
		}
	}
	return ItemNames;
}

void AElevenGameModeBase::AddNewItem(AItemActor* Item)
{
	if (Item)
	{
		ItemActors.AddUnique(Item);
		Item->OnDestroyedItem.AddDynamic(this, &AElevenGameModeBase::RemoveItem);
		Item->OnPutItem.AddDynamic(this, &AElevenGameModeBase::ReactionForItemOnPut);

		ReactionForItemOnAdded();
	}
}
