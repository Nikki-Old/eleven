// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "FuncLibrary/Types.h"
#include "DescriptionWidget.generated.h"

/**
 * 
 */

class UTextBlock;

UCLASS()
class ELEVEN_API UDescriptionWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	virtual bool Initialize() override;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* DescriptionTextBlock;

	void UpdateDescriptin(FActorInfo ActorInfo);
};
