// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "FuncLibrary/Types.h"

#include "CursorWidget.generated.h"

/**
 * 
 */

class UImage;
class UPaperSprite;

UCLASS()
class ELEVEN_API UCursorWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	virtual bool Initialize() override;

	UPROPERTY(meta = (BindWidget))
	UImage* CursorImage = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "CursorWidget")
	UPaperSprite* CursorDefault = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "CursorWidget")
	UPaperSprite* CursorInteract = nullptr;

	UFUNCTION(BlueprintCallable, Category = "CursorWidget")
	void SetCursorImageByState(EMouseState NewMouseState);

private:
	EMouseState CursorImageState = EMouseState::NONE_State;
};
