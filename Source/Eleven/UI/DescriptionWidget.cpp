// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/DescriptionWidget.h"
#include "Components/TextBlock.h"

bool UDescriptionWidget::Initialize()
{
	auto Result = Super::Initialize();

	return Result;
}

void UDescriptionWidget::UpdateDescriptin(FActorInfo ActorInfo)
{
	if (DescriptionTextBlock)
	{
		DescriptionTextBlock->SetText(ActorInfo.Description);
	}
}
