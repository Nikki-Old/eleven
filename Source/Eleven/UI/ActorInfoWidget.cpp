// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/ActorInfoWidget.h"
#include "Components/TextBlock.h"

bool UActorInfoWidget::Initialize()
{
	return Super::Initialize();
}

void UActorInfoWidget::UpdateInfo(FActorInfo NewActorInfo)
{
	ItemName->SetText(FText::FromString(NewActorInfo.Name.ToString()));
}
