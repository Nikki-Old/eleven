// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/CursorWidget.h"
#include "Components/Image.h"
#include "PaperSprite.h"

bool UCursorWidget::Initialize()
{
	auto Result = Super::Initialize();

	return Result;
}

void UCursorWidget::SetCursorImageByState(EMouseState NewMouseState)
{
	if (CursorImage && CursorImageState != NewMouseState)
	{
		UPaperSprite* NewCursorImage = nullptr;
		switch (NewMouseState)
		{
			case EMouseState::Default_State:
				NewCursorImage = CursorDefault;
				break;
			case EMouseState::Interact_State:
				NewCursorImage = CursorInteract;
				break;

			default:
				NewCursorImage = CursorDefault;
				break;
		}

		CursorImage->SetBrushFromAtlasInterface(NewCursorImage);
		CursorImageState = NewMouseState;
	}
}
