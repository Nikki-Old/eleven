// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "FuncLibrary/Types.h"

#include "ActorInfoWidget.generated.h"

/**
 * 
 */
class UTextBlock;

UCLASS()
class ELEVEN_API UActorInfoWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	/** Function for binds WidgetComonents. */
	virtual bool Initialize() override;

	UFUNCTION(BlueprintCallable, Category = "ItemActorWidget")
	void UpdateInfo(FActorInfo NewItemInfo);

protected:
	UPROPERTY(meta = (BindWidget))
	UTextBlock* ItemName = nullptr;
	
};
