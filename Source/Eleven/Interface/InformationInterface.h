// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "FuncLibrary/Types.h"
#include "InformationInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UInformationInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class ELEVEN_API IInformationInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Interact")
	FActorInfo GetActorInfo() const;
};
