// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "FuncLibrary/Types.h"
#include "PCBase.generated.h"

/**
 * 
 */

class UUserWidget;
class UCursorWidget;
class ACharacter2D;
class AInfoActor;

UCLASS()
class ELEVEN_API APCBase : public APlayerController
{
	GENERATED_BODY()

public:
	APCBase();

	// ~Overrides: APlayerController
	virtual void SetupInputComponent() override;

	virtual void Tick(float DeltaSeconds) override;

	UFUNCTION(BlueprintCallable, Category = "PC Base | Cursor")
	void SetMouseWidgetByState(EMouseState NewMouseState);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "PC Base | Cursor")
	TSubclassOf<UCursorWidget> CursorWidgetClass;

	UFUNCTION(BlueprintPure, Category = "PC Base")
	FHitResult GetHitUnderCursor() const;

	UFUNCTION()
	void NotifyCanInteractWithActor(AActor* NewUsageActor);

	UFUNCTION()
	void ClearInteractActor();

protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	void CursorTick(float DeltaTime);

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "PC Base | Interact")
	TSubclassOf<AInfoActor> InfoActorClass;

private:
	UCursorWidget* CurrentCursorWidget = nullptr;

	UPROPERTY()
	AInfoActor* ActorInfo = nullptr;

	UFUNCTION()
	void TryStartInteractWithActor();

	UFUNCTION()
	void TryStartUsageMainSlot();

	UFUNCTION()
	void StopUsageItem();

	UFUNCTION()
	void EndUsageItem(AActor* Actor);

	UFUNCTION()
	void TryStartInteract();

	UFUNCTION()
	void TryDropItem();

	UFUNCTION()
	void TrySwitchItem();

	UFUNCTION()
	void ShowActorInfo(FActorInfo ActorInfomation, FVector Location);

	AActor* InteractActor = nullptr;
	AActor* ForceInteractActor = nullptr;

	ACharacter2D* PawnCharacter2D = nullptr;

	FHitResult HitResultUnderCursor;

	AItemActor* UsageActor = nullptr;
	bool bIsStartUsageItem;
};
