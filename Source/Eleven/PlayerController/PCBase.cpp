// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerController/PCBase.h"
#include "Blueprint/UserWidget.h"

#include "ActorComponent/InventoryComponent.h"
#include "Character/Character2D.h"
#include "UI/CursorWidget.h"
#include "Interface/InteractInterface.h"
#include "Interface/InformationInterface.h"
#include "Actor/Item/ItemActor.h"
#include "Actor/Info/InfoActor.h"

APCBase::APCBase()
{
}

void APCBase::SetupInputComponent()
{
	// Always call this.
	Super::SetupInputComponent();

	InputComponent->BindAction("LeftMouse", IE_Pressed, this, &APCBase::TryStartInteractWithActor);
	InputComponent->BindAction("RightMouse", IE_Pressed, this, &APCBase::TryStartUsageMainSlot);
	InputComponent->BindAction("Interact", IE_Pressed, this, &APCBase::TryStartInteract);
	InputComponent->BindAction("DropItem", IE_Pressed, this, &APCBase::TryDropItem);
	InputComponent->BindAction("SwitchItem", IE_Pressed, this, &APCBase::TrySwitchItem);
}

void APCBase::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	CursorTick(DeltaSeconds);
}

void APCBase::SetMouseWidgetByState(EMouseState NewMouseState)
{
	if (IsValid(CursorWidgetClass))
	{
		if (!CurrentCursorWidget)
		{
			CurrentCursorWidget = CreateWidget<UCursorWidget>(this, CursorWidgetClass);

			// Set New Mouse Widget:
			this->SetMouseCursorWidget(EMouseCursor::Default, CurrentCursorWidget);
		}

		if (CurrentCursorWidget)
		{
			CurrentCursorWidget->SetCursorImageByState(NewMouseState);
		}
	}
}

void APCBase::CursorTick(float DeltaTime)
{
	if (!GetWorld())
	{
		return;
	}

	bool bIsDefault = true;
	// Get hit under cursor:
	this->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, HitResultUnderCursor);

	if (HitResultUnderCursor.bBlockingHit)
	{
		if (HitResultUnderCursor.Actor.IsValid())
		{
			AActor* HitActor = HitResultUnderCursor.Actor.Get();
			if (HitActor)
			{
				// Debug:
				//GEngine->AddOnScreenDebugMessage(0, 1.0f, FColor::Red, HitActor->GetName());

				// Hit Actor have Interface "InformationInterface"?
				if (HitActor->GetClass()->ImplementsInterface(UInformationInterface::StaticClass()))
				{
					ShowActorInfo(IInformationInterface::Execute_GetActorInfo(HitActor), HitResultUnderCursor.Location);
				}

				// Hit Actor have Interface "InteractInterface"?
				if (HitActor->GetClass()->ImplementsInterface(UInteractInterface::StaticClass()))
				{
					if (IInteractInterface::Execute_IsCanInteract(HitActor))
					{
						SetMouseWidgetByState(EMouseState::Interact_State);

						InteractActor = HitActor;

						bIsDefault = false;
					}
				}
			}
		}
	}

	if (bIsDefault)
	{
		SetMouseWidgetByState(EMouseState::Default_State);
		if (ActorInfo)
		{
			ActorInfo->Hide();
		}

		InteractActor = nullptr;
	}
}

void APCBase::TryStartInteractWithActor()
{
	if (bIsStartUsageItem)
	{
		if (UsageActor)
		{
			IUsageInterface::Execute_Usage(UsageActor);

			return;
		}
	}

	if (InteractActor)
	{
		IInteractInterface::Execute_StartInteract(InteractActor, GetPawn());

		return;
	}
}

void APCBase::TryStartUsageMainSlot()
{
	if (PawnCharacter2D)
	{
		if (!UsageActor && !bIsStartUsageItem)
		{
			// Take item from main slot:
			auto InventoryComponent = Cast<UInventoryComponent>(PawnCharacter2D->GetComponentByClass(UInventoryComponent::StaticClass()));
			if (InventoryComponent)
			{
				auto Item = InventoryComponent->GetItemFromMainSlot();
				if (Item)
				{
					bIsStartUsageItem = IUsageInterface::Execute_StartUsage(Item, GetPawn());

					if (bIsStartUsageItem)
					{
						UsageActor = Item;
						//UsageActor->StartUsage(GetPawn());
						Item->OnEndUsageItem.AddDynamic(this, &APCBase::EndUsageItem);
					}
				}
			}
		}

		if (UsageActor && !bIsStartUsageItem)
		{
			if (IUsageInterface::Execute_IsUse(UsageActor))
			{
				IUsageInterface::Execute_EndUsage(UsageActor);
			}
			else
			{
				bIsStartUsageItem = IUsageInterface::Execute_StartUsage(UsageActor, GetPawn());
			}
		}
	}
}

void APCBase::EndUsageItem(AActor* Actor)
{
	if (Actor && Actor == UsageActor)
	{
		UsageActor->OnEndUsageItem.RemoveDynamic(this, &APCBase::EndUsageItem);
		UsageActor = nullptr;
		bIsStartUsageItem = false;
	}
}

void APCBase::TryStartInteract()
{
	if (ForceInteractActor)
	{
		if (IInteractInterface::Execute_StartInteract(ForceInteractActor, GetPawn()))
		{
		}
	}
}

void APCBase::TryDropItem()
{
	if (HitResultUnderCursor.bBlockingHit)
	{
		if (PawnCharacter2D)
		{
			auto InventoryComponent = Cast<UInventoryComponent>(PawnCharacter2D->GetComponentByClass(UInventoryComponent::StaticClass()));
			if (InventoryComponent)
			{
				StopUsageItem();
				InventoryComponent->DropItemFromMainSlot(HitResultUnderCursor.Location);
			}
		}
	}
}

void APCBase::StopUsageItem()
{
	if (bIsStartUsageItem)
	{
		if (UsageActor)
		{
			IUsageInterface::Execute_EndUsage(UsageActor);
		}
	}
}
void APCBase::TrySwitchItem()
{
	if (PawnCharacter2D)
	{
		auto InventoryComponent = Cast<UInventoryComponent>(PawnCharacter2D->GetComponentByClass(UInventoryComponent::StaticClass()));
		if (InventoryComponent)
		{
			StopUsageItem();
			InventoryComponent->SwitchItemBetweenSlots();
		}
	}
}

void APCBase::ShowActorInfo(FActorInfo ActorInfomation, FVector Location)
{
	if (ActorInfo)
	{
		ActorInfo->SetActorLocation(Location);
		ActorInfo->Show(ActorInfomation);
	}
}

FHitResult APCBase::GetHitUnderCursor() const
{
	return HitResultUnderCursor;
}

void APCBase::NotifyCanInteractWithActor(AActor* NewUsageActor)
{
	ForceInteractActor = NewUsageActor;
}

void APCBase::ClearInteractActor()
{
	ForceInteractActor = nullptr;
}

void APCBase::BeginPlay()
{
	if (!GetWorld())
	{
		return;
	}

	Super::BeginPlay();

	// Set Default Mouse Widget:
	SetMouseWidgetByState(EMouseState::Default_State);
	this->bShowMouseCursor = true;

	PawnCharacter2D = Cast<ACharacter2D>(GetPawn());

	// Create InfoActor:
	ActorInfo = GetWorld()->SpawnActor<AInfoActor>(InfoActorClass, FTransform());
	ActorInfo->Hide();
}
