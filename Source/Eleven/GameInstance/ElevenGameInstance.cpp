// Fill out your copyright notice in the Description page of Project Settings.

#include "GameInstance/ElevenGameInstance.h"

bool UElevenGameInstance::GetItemInfoByName(FName ItemName, FItemActorInfo& OutInfo) const
{
	bool bIsFind = false;

	if (ItemInfoTable)
	{
		FItemActorInfo* ItemInfoRow;
		TArray<FName> RowNames = ItemInfoTable->GetRowNames();

		// Get information from table:
		int8 i = 0;
		while (i < RowNames.Num() && !bIsFind)
		{
			ItemInfoRow = ItemInfoTable->FindRow<FItemActorInfo>(RowNames[i], "");
			if (ItemInfoRow->DevItemName == ItemName)
			{
				OutInfo = *(ItemInfoRow);
				bIsFind = true;
			}

			i++;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, (TEXT("UTPSGameInstance::GetItemInfoByName - ItemInfoTable - NULL")));
	}

	return bIsFind;
}

void UElevenGameInstance::GetItemNames(int32 Count, TArray<FName>& OutNames)
{
	for (int32 i = 0; i < Count; i++)
	{
		const auto RandomIndex = FMath::RandRange(0, ItemNames.Num() - 1);

		if (ItemNames.IsValidIndex(RandomIndex))
		{
			OutNames.Add(ItemNames[RandomIndex]);
		}
	}
}

FItemActorInfo UElevenGameInstance::GetRandomItemInfo() const
{
	FItemActorInfo RandomItemInfo;

	if (ItemInfoTable)
	{
		const int32 Randomindex = FMath::RandRange(0, 19);

		TArray<FName> RowNames = ItemInfoTable->GetRowNames();

		if (RowNames.IsValidIndex(Randomindex))
		{

			const FName RandomName = RowNames[Randomindex];

			RandomItemInfo = *(ItemInfoTable->FindRow<FItemActorInfo>(RandomName, ""));
		}
	}

	return RandomItemInfo;
}
