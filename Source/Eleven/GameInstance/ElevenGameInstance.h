// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "FuncLibrary/Types.h"
#include "ElevenGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class ELEVEN_API UElevenGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintPure, Category = "ItemInfo")
	bool GetItemInfoByName(FName ItemName, FItemActorInfo& OutInfo) const;

	UPROPERTY(EditDefaultsOnly, Category = "ItemInfo")
	TArray<FName> ItemNames;
	
	UFUNCTION(BlueprintCallable, Category = "ItemInfo")
	void GetItemNames(int32 Count, TArray<FName>& OutNames);

	UFUNCTION(BlueprintCallable, Category = "ItemInfo")
	FItemActorInfo GetRandomItemInfo() const;
	
protected:
	// DT Item:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item")
	UDataTable* ItemInfoTable = nullptr;

	
};
