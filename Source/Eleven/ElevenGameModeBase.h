// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ElevenGameModeBase.generated.h"

/**
 * 
 */
class AItemActor;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnItemDisappeared);

UCLASS()
class ELEVEN_API AElevenGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable, EditAnyWhere, BlueprintReadWrite, Category = "GameMode")
	FOnItemDisappeared OnItemDisappeared;

	UFUNCTION(BlueprintCallable, Category = "GameMode")
	void AddNewItem(AItemActor* Item);

	UFUNCTION(BlueprintCallable, Category = "GameMode")
	void RemoveItem(AItemActor* Item);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "GameMode")
	void ReactionForItemOnPut();
	void ReactionForItemOnPut_Implementation();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "GameMode")
	void ReactionForItemOnAdded();
	void ReactionForItemOnAdded_Implementation();

protected:
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "GameMode | Order")
	TArray<FName> GetItemNames(int32 Count) const;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "GameMode")
	TArray<AItemActor*> ItemActors;
};
